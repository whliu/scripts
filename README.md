# CV IV Plots

This repo stores some useful scripts for plotting CV IV curves for 2 pads and 6 pads.

## 6-pads
Upload .iv and .cv files into the folder structure provided. For example, /10/Fresh/ should have all the .cv and .iv files for all the unirradiated sensor 10 data. /11/Irrad/ should have all the .cv and .iv files for the irradiated sensor 11 data.

Check the naming of the files - some adjustments will be needed with the scripts to parse the name correctly.