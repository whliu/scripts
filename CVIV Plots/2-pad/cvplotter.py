# This script plots the CV profiles of sensors measured before and after irradiation.
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import mplhep as hep
hep.style.use("CMS")

# Read the CSV file
file_path = "CV.csv"  # Replace with your CSV file path
df = pd.read_csv(file_path)

# Extract data for plotting
x = abs(df.iloc[:, 0])  # First column

# After Irradiation CV data
y1 = df.iloc[:, 1]  # Second column
y2 = df.iloc[:, 2]  # Third column
# add your data here

# Before Irradiation CV data
y3 = df.iloc[:, 3]  
y4 = df.iloc[:, 4]  
# add your data here

# Create a plot of before and after irradiated CV profiles on same plot.
plt.figure(figsize=(12, 10))
# Unirrated sensors as lines
plt.plot(x, 1/(1e12*y4)**2, 'r-', label=r'$S1\_L\ $', linewidth=4.0)
plt.plot(x, 1/(1e12*y3)**2, 'm-', label=r'$S1\_R\ $', linewidth=4.0)
# Irradiated sensors as circles
plt.scatter(x, 1/(1e12*y1)**2, s = 60,facecolors='none', edgecolors='r',linewidth=2.0, label=r'$S1\_L\ (0.5\times10^{15}\ \mathrm{n_{eq}/cm^{2}})$')
plt.scatter(x, 1/(1e12*y2)**2, s= 60,facecolors='none', edgecolors='m', linewidth=2.0, label=r'$S1\_R\ (0.5\times10^{15}\ \mathrm{n_{eq}/cm^{2}})$')

csfont = {'fontname':'Times New Roman', 'fontsize': 38}
hfont = {'fontname':'Times New Roman', 'fontsize': 42}
custom_font = FontProperties(family='Times New Roman', size=20)

legend = plt.legend(prop=custom_font, loc='upper left')
legend.get_frame().set_facecolor('white')
legend.get_frame().set_alpha(1)
legend.get_frame().set_edgecolor('black')

# Add labels and legend
plt.xlabel("Voltage [V]",**hfont)
plt.ylabel("Capacitance\u207B\u00B2 [pF\u207B\u00B2]",**hfont)

# Increase axes font size
plt.tick_params(axis='both', which='major', labelsize=30)  # Increase labelsize as needed

# Add cms text
hep.cms.label(llabel="Preliminary", rlabel='',loc=1, data=False, ax=None, fontsize=24, com=7)
plt.title("Plot of Inverse Squared Capacitance against Voltage",**csfont)
plt.tight_layout()

# Show the plot
plt.show()

