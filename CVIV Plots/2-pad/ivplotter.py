# This script plots the data from IV pad current measurements.
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import mplhep as hep
hep.style.use("CMS")

# Read the CSV file
file_path = "IV.csv"  # Replace with your CSV file path
df = pd.read_csv(file_path)

# Extract data for plotting
x = abs(df.iloc[:, 0])  # First column
x1 = abs(df.iloc[1:85, 0])  # First column shortened to display current before large spikes

# After Irradiation IV data
y1 = abs(df.iloc[:, 1])  # Second column
y2 = abs(df.iloc[:, 2])  # Third column
# add your data here

# Before Irradiation IV data
y3 = abs(df.iloc[:, 3])  
y4 = abs(df.iloc[:, 4])  
# add your data here

csfont = {'fontname':'Times New Roman', 'fontsize': 42}
hfont = {'fontname':'Times New Roman', 'fontsize': 42}
custom_font = FontProperties(family='Times New Roman', size=30)

# Create two plots - one for sensors before irradiation, and one for sensors after irradiation.
plt.figure()
plt.scatter(x, y1, s=60, facecolors='none', edgecolors='r',linewidth=2.0, label="S1_Left (0.5E15 eq/cm\u00B2)")
plt.plot(x, y2, 'yx', markersize=10, label="S3_Right (2.5E15 eq/cm\u00B2)")

# Add CMS text to the plot
hep.cms.label(llabel="Preliminary", rlabel='',loc=1, data=False, ax=None, fontsize=24, com=7)
plt.xlabel("Voltage [V]", **hfont)
plt.ylabel("Pad Current [$\mu$A]", **hfont)

# Increase font size of axes labels
plt.tick_params(axis='both', which='major', labelsize=30)  # Increase labelsize as needed

plt.title("Plot of Pad Current against Voltage", **csfont)
plt.tight_layout()
plt.legend(prop=custom_font, loc='best')

plt.figure()
plt.plot(x, y3, 'r-', label="S1_Left", linewidth=4.0)
plt.plot(x, y4, 'm-', label="S1_Right" , linewidth=4.0)

# Add CMS text to the plot and labels
hep.cms.label(llabel="Preliminary", rlabel='',loc=1, data=False, ax=None, fontsize=24, com=7)
plt.xlabel("Voltage [V]", **hfont)
plt.ylabel("Pad Current [pA]", **hfont)


# Increase font size of axes labels
plt.tick_params(axis='both', which='major', labelsize=30)  # Increase labelsize as needed

plt.title("Plot of Pad Current against Voltage",**csfont)
plt.tight_layout()
plt.legend(prop=custom_font, loc = 'best')

# Show the plot
plt.show()