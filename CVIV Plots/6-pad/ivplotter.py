### script to read .iv files for two 6-pad sensors and plot their current against voltage profiles.
### Makes two plots, one for unirradiated data, another with irradiated data

import os
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

def parse_iv_file(file_path):
    """Parse .iv file and return Voltage and Pad Current data."""
    with open(file_path, 'r') as file:
        lines = file.readlines()

        # Find the BEGIN marker to locate data start
        begin_index = next(i for i, line in enumerate(lines) if line.strip() == "BEGIN") + 1

        # Extract data rows
        data = [line.strip().split() for line in lines[begin_index:] if line.strip() != "END"]

        # Convert to DataFrame
        df = pd.DataFrame(data, columns=["Voltage", "Total_Current", "Pad_Current"])
        df = df.astype(float)  # Convert to float for calculations
        return df["Voltage"], df["Pad_Current"]

# Folder structure
base_paths = ["./10", "./11"]  # Replace with the base paths to the sensor folders
conditions = ["Fresh", "Irrad"]

# Define colors and markers for each pad and sensor
pad_colors = {
    "pad1": "b",
    "pad2": "g",
    "pad3": "r",
    "pad4": "c",
    "pad5": "m",
    "pad6": "y"
}
sensor_markers = {
    "10": "o",
    "11": "D"
}

# Plot unirradiated data
plt.figure(figsize=(10, 8))
font_properties = FontProperties(family='Times New Roman', size=12)

for base_path in base_paths:
    sensor_name = os.path.basename(base_path)

    condition_path = os.path.join(base_path, "Fresh")

    if not os.path.exists(condition_path):
        continue  # Skip if the condition path does not exist

    for file_name in os.listdir(condition_path):
        if file_name.endswith(".iv"):
            # Extract pad name from file name assuming format includes "padX"
            pad_name = next((key for key in pad_colors.keys() if key in file_name.lower()), None)
            if pad_name is None:
                continue  # Skip files without matching pad names

            file_path = os.path.join(condition_path, file_name)

            # Parse .iv file
            Voltage, Pad_Current = parse_iv_file(file_path)

            # Plot data
            plt.plot(-Voltage, -Pad_Current * 1e6, label=f"Sensor {sensor_name} {pad_name} (Fresh)", linewidth=1.5, color=pad_colors[pad_name])

# Add labels, legend, and title for unirradiated plot
plt.legend(prop=font_properties, loc='upper left', ncol=2, fontsize=8)
plt.xlabel("Voltage [V]", fontproperties=font_properties)
plt.ylabel("Pad Current [$\mu$A]", fontproperties=font_properties)
plt.tick_params(axis='both', which='major', labelsize=10)
plt.title("Pad Current vs Voltage (Unirradiated Sensors 10 and 11)", fontproperties=font_properties)
plt.tight_layout()
plt.show()

# Plot irradiated data
plt.figure(figsize=(10, 8))

for base_path in base_paths:
    sensor_name = os.path.basename(base_path)

    condition_path = os.path.join(base_path, "Irrad")

    if not os.path.exists(condition_path):
        continue  # Skip if the condition path does not exist

    for file_name in os.listdir(condition_path):
        if file_name.endswith(".iv"):
            # Extract pad name from file name assuming format includes "padX"
            pad_name = next((key for key in pad_colors.keys() if key in file_name.lower()), None)
            if pad_name is None:
                continue  # Skip files without matching pad names

            file_path = os.path.join(condition_path, file_name)

            # Parse .iv file
            Voltage, Pad_Current = parse_iv_file(file_path)

            # Plot data with different markers for each sensor
            dose_label = "8e14 p_eq/cm^2" if sensor_name == "10" else "1.6e15 p_eq/cm^2"
            plt.scatter(
                -Voltage,
                -Pad_Current * 1e6,
                label=f"Sensor {sensor_name} {pad_name} ({dose_label})",
                s=30,
                facecolors=pad_colors[pad_name],
                edgecolors=pad_colors[pad_name],
                marker=sensor_markers[sensor_name],
                linewidth=1.0
            )

# Add labels, legend, and title for irradiated plot
plt.legend(prop=font_properties, loc='upper left', ncol=2, fontsize=8)
plt.xlabel("Voltage [V]", fontproperties=font_properties)
plt.ylabel("Pad Current [$\mu$A]", fontproperties=font_properties)
plt.tick_params(axis='both', which='major', labelsize=10)
plt.title("Pad Current vs Voltage (Irradiated Sensors 10 and 11)", fontproperties=font_properties)
plt.tight_layout()
plt.show()
