### plots cv curves for two sensors, before and after irradiation.
### two plots, one for sensor 10, one for sensor 11

import os
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import numpy as np

def parse_cv_file(file_path):
    """Parse .cv file and return V_detector and Capacitance data."""
    with open(file_path, 'r') as file:
        lines = file.readlines()

        # Find the BEGIN marker to locate data start
        begin_index = next(i for i, line in enumerate(lines) if line.strip() == "BEGIN") + 1

        # Extract data rows
        data = [line.strip().split() for line in lines[begin_index:] if line.strip() != "END"]

        # Convert to DataFrame
        df = pd.DataFrame(data, columns=["V_detector", "Capacitance", "Conductivity", "Bias", "Current"])
        df = df.astype(float)  # Convert to float for calculations
        return df["V_detector"], df["Capacitance"]

def gaussian_smearing(data, sigma=1):
    """Apply Gaussian smoothing to data."""
    kernel = np.exp(-0.5 * (np.arange(-3, 4) / sigma)**2)
    kernel /= kernel.sum()
    smoothed = np.convolve(data, kernel, mode='same')
    smoothed[:3] = data[:3]  # Avoid edge effects at the start
    smoothed[-3:] = data[-3:]  # Avoid edge effects at the end
    return smoothed

# Folder structure
base_paths = ["./10", "./11"]  # Replace with the base paths to the sensor folders
conditions = ["Fresh", "Irrad"]

# Define colors and markers for each pad and sensor
pad_colors = {
    "pad1": "b",
    "pad2": "g",
    "pad3": "r",
    "pad4": "c",
    "pad5": "m",
    "pad6": "y"
}
sensor_markers = {
    "Fresh": "-",
    "Irrad": "x"
}
sensor_doses = {
    "10": "8e14 p_eq/cm^2",
    "11": "1.6e15 p_eq/cm^2"
}

# Create separate plots for each sensor
for base_path in base_paths:
    sensor_name = os.path.basename(base_path)
    plt.figure(figsize=(12, 10))
    font_properties = FontProperties(family='Times New Roman', size=12)

    for condition in conditions:
        condition_path = os.path.join(base_path, condition)

        if not os.path.exists(condition_path):
            continue  # Skip if the condition path does not exist

        for file_name in os.listdir(condition_path):
            if file_name.endswith(".cv"):
                # Extract pad name from file name assuming format includes "padX"
                pad_name = next((key for key in pad_colors.keys() if key in file_name.lower()), None)
                if pad_name is None:
                    continue  # Skip files without matching pad names

                file_path = os.path.join(condition_path, file_name)

                # Parse .cv file
                V_detector, Capacitance = parse_cv_file(file_path)

                # Plot data with different styles for each condition
                label_suffix = f" (Fresh)" if condition == "Fresh" else f" (Irradiated, {sensor_doses[sensor_name]})"
                if condition == "Fresh":
                    smoothed_data = gaussian_smearing(1 / (1e12 * Capacitance) ** 2)
                    plt.plot(
                        -V_detector,
                        smoothed_data,
                        label=f"Sensor {sensor_name} {pad_name}{label_suffix}",
                        linewidth=1.5,
                        color=pad_colors[pad_name]
                    )
                else:
                    plt.scatter(
                        -V_detector,
                        1 / (1e12 * Capacitance) ** 2,
                        label=f"Sensor {sensor_name} {pad_name}{label_suffix}",
                        s=30,
                        color=pad_colors[pad_name],
                        marker=sensor_markers[condition],
                        linewidth=1.0
                    )

    # Add labels, legend, and title
    plt.legend(prop=font_properties, loc='upper left', ncol=2, fontsize=8)
    plt.xlabel("Voltage [V]", fontproperties=font_properties)
    plt.ylabel("Capacitance⁻² [pF⁻²]", fontproperties=font_properties)
    plt.tick_params(axis='both', which='major', labelsize=10)
    plt.title(f"Inverse Squared Capacitance vs Voltage for Sensor {sensor_name}", fontproperties=font_properties)
    plt.tight_layout()
    plt.show()
