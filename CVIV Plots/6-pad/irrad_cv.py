### Plots irradiated CV curves for two sensors

import os
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

def parse_cv_file(file_path):
    """Parse .cv file and return V_detector and Capacitance data."""
    with open(file_path, 'r') as file:
        lines = file.readlines()

        # Find the BEGIN marker to locate data start
        begin_index = next(i for i, line in enumerate(lines) if line.strip() == "BEGIN") + 1

        # Extract data rows
        data = [line.strip().split() for line in lines[begin_index:] if line.strip() != "END"]

        # Convert to DataFrame
        df = pd.DataFrame(data, columns=["V_detector", "Capacitance", "Conductivity", "Bias", "Current"])
        df = df.astype(float)  # Convert to float for calculations
        return df["V_detector"], df["Capacitance"]

# Folder structure
base_paths = ["./10", "./11"]  # Replace with the base paths to the sensor folders
condition = "Irrad"  # Only focus on irradiated sensors

# Define colors and markers for each pad and sensor
pad_colors = {
    "pad1": "b",
    "pad2": "g",
    "pad3": "r",
    "pad4": "c",
    "pad5": "m",
    "pad6": "y"
}
sensor_markers = {
    "10": "-",
    "11": "o"
}

# Initialize plot
plt.figure(figsize=(14, 12))  # Larger figure size for better visibility
font_properties = FontProperties(family='Times New Roman', size=14)

# Iterate over sensors
for base_path in base_paths:
    sensor_name = os.path.basename(base_path)

    condition_path = os.path.join(base_path, condition)

    if not os.path.exists(condition_path):
        continue  # Skip if the condition path does not exist

    for file_name in os.listdir(condition_path):
        if file_name.endswith(".cv"):
            # Extract pad name from file name assuming format includes "padX"
            pad_name = next((key for key in pad_colors.keys() if key in file_name.lower()), None)
            if pad_name is None:
                continue  # Skip files without matching pad names

            file_path = os.path.join(condition_path, file_name)

            # Parse .cv file
            V_detector, Capacitance = parse_cv_file(file_path)

            # Plot data with different markers for each sensor
            dose_label = "8e14 p_eq/cm^2" if sensor_name == "10" else "1.6e15 p_eq/cm^2"
            if sensor_name == "10":
                plt.plot(
                    -V_detector,
                    1 / (1e12 * Capacitance) ** 2,
                    label=f"Sensor {sensor_name} {pad_name} ({dose_label})",
                    linewidth=2.5,  # Thicker line for better visibility
                    color=pad_colors[pad_name],
                    linestyle="-"
                )
            else:
                plt.scatter(
                    -V_detector,
                    1 / (1e12 * Capacitance) ** 2,
                    label=f"Sensor {sensor_name} {pad_name} ({dose_label})",
                    s=50,  # Larger marker size for better visibility
                    color=pad_colors[pad_name],
                    marker=sensor_markers[sensor_name],
                    linewidth=1.5
                )

# Add labels, legend, and title
plt.legend(prop=font_properties, loc='lower right', ncol=2, fontsize=10)
plt.xlabel("Voltage [V]", fontproperties=font_properties)
plt.ylabel("Capacitance⁻² [pF⁻²]", fontproperties=font_properties)
plt.tick_params(axis='both', which='major', labelsize=12)
plt.title("Inverse Squared Capacitance vs Voltage (Irradiated Sensors 10 and 11)", fontproperties=font_properties)
plt.tight_layout()
plt.show()
